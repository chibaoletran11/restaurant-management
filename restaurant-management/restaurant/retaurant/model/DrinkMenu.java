package retaurant.model;

public class DrinkMenu extends Menu {
	private boolean isAlcohol;

	public DrinkMenu(int id, String name, String description, String image, double price, boolean isAlcohol) {
		super(id, name, description, image, price);
		this.isAlcohol = isAlcohol;
	}

	@Override
	public String toString() {
		return "DinnerMenu [ getName()=" + getName() + ", getDescription()=" + getDescription() + ", getImage()="
				+ getImage() + ", getPrice()=" + getPrice() + ", getId()=" + getId() + "]";
	}
}