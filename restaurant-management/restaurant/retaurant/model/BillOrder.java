package retaurant.model;

public class BillOrder {
	private int quantity;
	private Bill bill;
	private Menu menu;
	
	public BillOrder(int quantity, Bill bill, Menu menu) {

		this.quantity = quantity;
		this.bill = bill;
		this.menu = menu;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	
	}


