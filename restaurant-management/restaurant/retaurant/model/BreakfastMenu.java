package retaurant.model;

public class BreakfastMenu extends FoodMenu {

	public BreakfastMenu(int id, String name, String description, String image, double price, boolean starch) {
		super(id, name, description, image, price, starch);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "BreakfastMenu [isStarch=" + isStarch + ", id=" + id + ", name=" + name + ", description=" + description
				+ ", image=" + image + ", price=" + price + "]";
	}

}
