package retaurant.model;

public class FoodMenu extends Menu {
	protected boolean isStarch;

	public FoodMenu(int id, String name, String description, String image, double price, boolean starch) {
		super(id, name, description, image, price);
		this.isStarch = starch;

	}

	@Override
	public String toString() {
		return "FoodMenu [isStarch=" + isStarch + ", id=" + id + ", name=" + name + ", description=" + description
				+ ", image=" + image + ", price=" + price + "]";
	}

}
