 package retaurant.model;

import java.util.List;

public class Bill {
	private int id;
	private String orderTimed;
	private List<BillOrder> billOrder;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderTimed() {
		return orderTimed;
	}

	public void setOrderTimed(String orderTimed) {
		this.orderTimed = orderTimed;
	}
}