package retaurant.model;

public class DinnerMenu extends FoodMenu {

	public DinnerMenu(int id, String name, String description, String image, double price, boolean starch) {
		super(id, name, description, image, price, starch);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "DinnerMenu [isStarch=" + isStarch + ", id=" + id + ", name=" + name + ", description=" + description
				+ ", image=" + image + ", price=" + price + "]";
	}

}
