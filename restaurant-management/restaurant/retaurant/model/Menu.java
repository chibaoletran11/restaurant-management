package retaurant.model;

import java.io.Serializable;

public abstract class Menu implements Serializable {
	protected int id;
	protected String name;
	protected String description;
	protected String image;
	protected double price;

	public Menu() {

	}

	public Menu(int id, String name, String description, String image, double price) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.image = image;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Menu [name=" + name + ", description=" + description + ", image=" + image + ", price=" + price + "]";
	}

}
