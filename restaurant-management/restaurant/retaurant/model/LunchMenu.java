package retaurant.model;

public class LunchMenu extends FoodMenu {

	public LunchMenu(int id, String name, String description, String image, double price, boolean starch) {
		super(id, name, description, image, price, starch);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "LunchMenu [isStarch=" + isStarch + ", id=" + id + ", name=" + name + ", description=" + description
				+ ", image=" + image + ", price=" + price + "]";
	}

}