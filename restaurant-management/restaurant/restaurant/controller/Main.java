package restaurant.controller;

import java.io.File;
import java.util.Scanner;

import retaurant.model.BreakfastMenu;
import retaurant.model.DinnerMenu;
import retaurant.model.DrinkMenu;
import retaurant.model.FoodMenu;
import retaurant.model.LunchMenu;
import retaurant.model.Menu;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		RestaurantManagement restaurantManagement = new RestaurantManagement();

		while (true) {
			System.out.println("------Menu-------");
			System.out.println("1.Add Menu");
			System.out.println("2.Edit Menu");
			System.out.println("3.Delete Menu");
			System.out.println("4.Show Menu");

			String line = scanner.nextLine();

			switch (line) {
			case "1": {
				System.out.println("Enter a to insert Food");
				System.out.println("Enter b to insert Drink");

				String type = scanner.nextLine();
				switch (type) {
				case "a":
					System.out.println("Enter a1 to insert Breakfast");
					System.out.println("Enter a2 to insert Lunch");

					System.out.println("Enter a3 to insert Dinner");

					String food = scanner.nextLine();

					switch (food) {
					case "a1": {
						System.out.println("Insert Food Id");
						int id = scanner.nextInt();
						System.out.println("Insert Food name ");
						scanner.nextLine();
						String name = scanner.nextLine();
						System.out.println("Insert Description");
						String description = scanner.nextLine();
						System.out.println("Insert Image URL");
						String image = scanner.nextLine();
						System.out.println("Insert Price");
						double price = scanner.nextDouble();
						System.out.println("Is stard");
						boolean isStarch = scanner.nextBoolean();

						Menu breakfast = new BreakfastMenu(id, name, description, image, price, isStarch);
						restaurantManagement.addMenu(breakfast);

						System.out.println(breakfast.toString());
						break;
					}
					case "a2": {
						System.out.println("Insert Food Id");
						int id = scanner.nextInt();
						System.out.println("Insert Food name ");
						scanner.nextLine();

						String name = scanner.nextLine();
						System.out.println("Insert Description");
						String description = scanner.nextLine();
						System.out.println("Insert Image URL");
						String image = scanner.nextLine();
						System.out.println("Insert Price");
						double price = scanner.nextDouble();
						System.out.println("Is stard");
						boolean isStarch = scanner.nextBoolean();

						Menu lunch = new LunchMenu(id, name, description, image, price, isStarch);
						restaurantManagement.addMenu(lunch);

						System.out.println(lunch.toString());
						break;
					}
					case "a3": {
						System.out.println("Insert Food Id");
						int id = scanner.nextInt();
						System.out.println("Insert Food name ");
						scanner.nextLine();

						String name = scanner.nextLine();
						System.out.println("Insert Description");
						String description = scanner.nextLine();
						System.out.println("Insert Image URL");
						String image = scanner.nextLine();
						System.out.println("Insert Price");
						double price = scanner.nextDouble();
						System.out.println("Is stard");
						boolean isStarch = scanner.nextBoolean();
						Menu dinner = new DinnerMenu(id, name, description, image, price, isStarch);
						restaurantManagement.addMenu(dinner);
						System.out.println(dinner.toString());
						break;
					}
					default:
						System.out.println("Invalid");
						break;
					}
					break;

				case "b": {
					System.out.println("Insert Drink Id");
					int id = scanner.nextInt();
					System.out.println("Insert Drink name ");
					scanner.nextLine();

					String name = scanner.nextLine();
					System.out.println("Insert Description");
					String description = scanner.nextLine();
					System.out.println("Insert Image URL");
					String image = scanner.nextLine();
					System.out.println("Insert Price");
					double price = scanner.nextDouble();
					System.out.println("Is Alcohol");
					boolean isAlcohol = scanner.nextBoolean();
					Menu drink = new DrinkMenu(id, name, description, image, price, isAlcohol);
					restaurantManagement.addMenu(drink);

					System.out.println(drink.toString());
					break;

				}
				default:
					System.out.println("Invalid");
					break;
				}
				break;
			}
			case "2": {
				System.out.println("Insert Menu Id to Edit");
				int id = scanner.nextInt();
				System.out.println("Insert new Name");
				scanner.nextLine();

				String name = scanner.nextLine();
				System.out.println("Insert new Description");
				String description = scanner.nextLine();

				System.out.println("Insert new Image");
				String image = scanner.nextLine();
				System.out.println("Insert new Price");
				double price = scanner.nextDouble();

				restaurantManagement.editMenu(id, name, image, description, price);

				System.out.println("Item edited");
				break;

			}
			case "3": {
				System.out.println("Show Menu");
				restaurantManagement.showMenu();
				break;

			}

			}

		}
	}
}
