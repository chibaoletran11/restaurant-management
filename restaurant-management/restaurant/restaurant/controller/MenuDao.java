package restaurant.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import retaurant.model.Menu;

public class MenuDao {
	public static final String MENU_FILE_NAME = "menu.txt";

	public static boolean write(StringBuilder strbd) {
		FileOutputStream fileOutStream = null;
		BufferedOutputStream bufferOutStream = null;

		try {
			fileOutStream = new FileOutputStream(MENU_FILE_NAME);
			bufferOutStream = new BufferedOutputStream(fileOutStream);
			// convert data: string to byte array
			byte byteData[] = strbd.toString().getBytes();
			// write data to file
			bufferOutStream.write(byteData);
			bufferOutStream.flush();
			// return true if write file success
			return true;
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				bufferOutStream.close();
				fileOutStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public List<Menu> read() {
		List<Menu> menuList = new ArrayList<>();
		FileInputStream fileInputStream = null;
		ObjectInputStream objectInputStream = null;

		try {
			fileInputStream = new FileInputStream(new File(MENU_FILE_NAME));
			objectInputStream = new ObjectInputStream(fileInputStream);
			menuList = (List<Menu>) objectInputStream.readObject();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			closeStream(fileInputStream);
			closeStream(objectInputStream);
		}
		return menuList;
	}

	private void closeStream(InputStream is) {
		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private void closeStream(OutputStream os) {
		if (os != null) {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

}
